import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import cors from 'cors'
import dotenv from 'dotenv'
import logger from 'morgan'
import routes from './start/routes'

dotenv.config()

const app = express()
const PORT = process.env.PORT || '4000'
const uri = process.env.NODE_ENV === 'test' ? process.env.TEST_URI : process.env.ATLAS_URI

if (!process.env.NODE_ENV === 'test') app.use(logger('dev'))
app.use(cors())
app.use(bodyParser.json())
app.use('/', routes)

mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: true })

const { connection } = mongoose

connection.once('open', () => {
  if (!process.env.NODE_ENV === 'test') console.log('MongoDB connected')
})

const server = app.listen(PORT, () => console.log(`Server running on port ${PORT}`))

module.exports = server
