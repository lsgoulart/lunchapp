import express from 'express'
import RestaurantsController from '../../app/Controllers/Http/RestaurantsController'
import AuthMiddleware from '../../app/Middleware/auth'

const router = express.Router()

router.get('/', RestaurantsController.index)
router.post('/create', RestaurantsController.create)
router.get('/options', [AuthMiddleware], RestaurantsController.options)
router.post('/vote', [AuthMiddleware], RestaurantsController.vote)

export default router
