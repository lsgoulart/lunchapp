import express from 'express'
import UserController from '../../app/Controllers/Http/UserController'

const router = express.Router()

router.get('/profile', UserController.profile)
router.post('/login', UserController.login)
router.post('/register', UserController.register)

export default router
