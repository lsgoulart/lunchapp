import express from 'express'
import users from './users'
import restaurants from './restaurants'

const router = express.Router()

router.use('/user', users)
router.use('/restaurants', restaurants)

export default router
