process.env.NODE_ENV = 'test'

import 'babel-polyfill'

import mongoose from 'mongoose'
import User from '../../../app/Models/User'

import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../index'
let should = chai.should()

chai.use(chaiHttp)

describe('Users', () => {
  beforeEach(done => {
    //Before each test we empty the database
    User.deleteMany({}, () => {
      done()
    })
  })

  describe('/POST Register new User', () => {
    it('it should create a new user', done => {
      const newUser = {
        username: 'Faminto 1',
        email: 'faminto1@gmail.com',
        password: 'tocomfome'
      }

      chai
        .request(server)
        .post('/user/register')
        .send(newUser)
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('credentials')
          res.body.credentials.should.have.property('token')
          done()
        })
    })
  })

  describe('/POST Login', () => {
    it('it should authenticate a user', done => {
      const newUser = {
        username: 'Faminto 1',
        email: 'faminto1@gmail.com',
        password: 'tocomfome'
      }

      chai
        .request(server)
        .post('/user/register')
        .send(newUser)
        .end((err, res) => {
          res.should.have.status(200)
          chai
            .request(server)
            .post('/user/login')
            .send({
              email: 'faminto1@gmail.com',
              password: 'tocomfome'
            })
            .end((err, res) => {
              res.should.have.status(200)
              res.body.should.be.a('object')
              res.body.should.have.property('credentials')
              res.body.credentials.should.have.property('token')
              done()
            })
        })
    })
  })
})
