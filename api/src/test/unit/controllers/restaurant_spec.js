process.env.NODE_ENV = 'test'

import 'babel-polyfill'

import mongoose from 'mongoose'
import History from '../../../app/Models/History'
import Restaurant from '../../../app/Models/Restaurant'
import User from '../../../app/Models/User'

import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../index'
let should = chai.should()

chai.use(chaiHttp)

const createUser = async () => {
  const newUser = {
    username: 'Faminto 1',
    email: 'faminto1@gmail.com',
    password: 'tocomfome'
  }

  return chai
    .request(server)
    .post('/user/register')
    .send(newUser)
}

const createRestaurant = async () => {
  const Restaurant = {
    name: 'Club Life To Go',
    description:
      'Espere mais que uma alimentação saudável.  O Club Life To Go traz um conceito totalmente inovador: Healthy Fresh Food. Uma opção de alimentação rápida, prática e saudável para quem está na correria do dia a dia e não tem muito tempo para se alimentar de forma correta. Agora não tem mais desculpa para não se alimentar bem fora de casa. Com opções de comidas sem glúten, sem lactose e veganas, sem perder o sabor gourmet de uma boa refeição.',
    price: 13.2,
    image: 'https://media-cdn.tripadvisor.com/media/photo-s/11/52/ca/e6/o-menu-muda-diariamente.jpg'
  }

  return chai
    .request(server)
    .post('/restaurants/create')
    .send(Restaurant)
}

describe('Restaurants', () => {
  beforeEach(async () => {
    //Before each test we empty the database
    await History.deleteMany({})
    await Restaurant.deleteMany({})
    await User.deleteMany({})
  })

  describe('/POST Register new Restaurant', () => {
    it('it should create a new restaurant', async () => {
      const res = await createRestaurant()
      res.should.have.status(200)
      res.body.should.be.a('object')
      res.body.should.have.property('restaurant')
      res.body.restaurant.should.be.a('object')
    })
  })

  describe('/GET Get list of restaurants available', () => {
    it('it should get unauthenticated status', async () => {
      const res = await chai.request(server).get('/restaurants/options')
      res.should.have.status(401)
    })

    it('it should create and return a list of options for today', async () => {
      const user = await createUser()
      await createRestaurant()

      const list = await chai
        .request(server)
        .get('/restaurants/options')
        .set('authorization', `Bearer ${user.body.credentials.token}`)

      list.should.have.status(200)
      list.body.should.have.property('currentOptions')
      list.body.currentOptions.should.be.a('object')
    })

    it('it should register a vote on a chosen restaurant', async () => {
      const user = await createUser()
      const restaurant = await createRestaurant()

      const list = await chai
        .request(server)
        .get('/restaurants/options')
        .set('authorization', `Bearer ${user.body.credentials.token}`)

      const vote = await chai
        .request(server)
        .post('/restaurants/vote')
        .set('authorization', `Bearer ${user.body.credentials.token}`)
        .send({
          restaurantId: restaurant.body.restaurant._id
        })

      vote.should.have.status(200)
    })

    it('it should get an error on voting on same restaurant', async () => {
      const user = await createUser()
      const restaurant = await createRestaurant()

      const list = await chai
        .request(server)
        .get('/restaurants/options')
        .set('authorization', `Bearer ${user.body.credentials.token}`)

      await chai
        .request(server)
        .post('/restaurants/vote')
        .set('authorization', `Bearer ${user.body.credentials.token}`)
        .send({
          restaurantId: restaurant.body.restaurant._id
        })

      const vote = await chai
        .request(server)
        .post('/restaurants/vote')
        .set('authorization', `Bearer ${user.body.credentials.token}`)
        .send({
          restaurantId: restaurant.body.restaurant._id
        })
      vote.should.have.status(400)
      vote.error.text.should.be.equal('{"error":"You already chose a restaurant today"}')
    })
  })
})
