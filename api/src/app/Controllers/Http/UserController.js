import bcrypt from 'bcrypt'
import Joi from 'joi'

import User from '../../Models/User'
import { validateAsync, generateToken } from '../../../helpers'

export default {
  async register(req, res) {
    const { email } = req.body
    const schema = Joi.object().keys({
      username: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .min(6)
        .required()
    })

    const validate = await validateAsync(req.body, schema)

    if (validate.fails) {
      return res.status(400).send({
        error: validate.messages
      })
    }

    try {
      if (await User.findOne({ email }))
        return res.status(400).send({ error: 'User already exists' })

      const user = await User.create(req.body)

      user.password = undefined

      return res.send({
        user,
        credentials: {
          token: generateToken({ id: user.id, username: user.username, email: user.email }),
          type: 'Bearer'
        }
      })
    } catch (error) {
      return res.status(400).send({ error: 'Registration failed' })
    }
  },

  async login(req, res) {
    const { email, password } = req.body
    const schema = Joi.object().keys({
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string().min(6)
    })

    const validate = await validateAsync(req.body, schema)

    if (validate.fails) {
      return res.status(400).send({
        error: validate.messages
      })
    }

    const user = await User.findOne({ email }).select('+password')

    if (!user) return res.status(400).send({ error: 'User not found' })

    if (!(await bcrypt.compare(password, user.password)))
      return res.status(401).send({ error: 'Invalid username/password' })

    user.password = undefined

    return res.send({
      user,
      credentials: {
        token: generateToken({ id: user.id, username: user.username, email: user.email }),
        type: 'Bearer'
      }
    })
  },

  async profile(req, res) {}
}
