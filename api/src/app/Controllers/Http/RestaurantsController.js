import Joi from 'joi'
import { startOfWeek, endOfDay, subDays, startOfDay } from 'date-fns'

import Restaurant from '../../Models/Restaurant'
import History from '../../Models/History'
import { validateAsync } from '../../../helpers'

const getToday = () => {
  const start = new Date()
  start.setHours(0, 0, 0, 0)

  const end = new Date()
  end.setHours(23, 59, 59, 999)

  return { start, end }
}

const selectWinner = restaurants => {
  const orderByVotes = restaurants.sort((a, b) => b.votes.length - a.votes.length)[0]
  delete orderByVotes.votes
  return orderByVotes
}

const selectLastWinners = async () => {
  const weekList = await History.find({
    createdAt: {
      $gte: startOfWeek(new Date(), { weekStartsOn: 1 }),
      $lte: endOfDay(subDays(new Date(), 1))
    }
  }).exec()

  return weekList.map(item => {
    return selectWinner(item.restaurants)
  })
}

const mergeCurrentWithWinners = async currentOptions => {
  const lastWinners = await selectLastWinners()

  const restaurants = currentOptions.restaurants.map(item => {
    return {
      votes: item.votes,
      createdAt: item.createdAt,
      _id: item._id,
      name: item.name,
      description: item.description,
      price: item.price,
      image: item.image,
      active: !lastWinners.find(winner => `${winner._id}` === `${item._id}`)
    }
  })

  return restaurants
}

export default {
  async index(req, res) {
    const restaurants = await Restaurant.find().exec()

    return res.status(200).send({
      restaurants
    })
  },

  async create(req, res) {
    const { name } = req.body
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().required(),
      image: Joi.string()
    })

    const validate = await validateAsync(req.body, schema)

    if (validate.fails) {
      return res.status(400).send({
        error: validate.messages
      })
    }

    try {
      if (await Restaurant.findOne({ name }))
        return res.status(400).send({ error: 'Restaurant already exists' })

      const restaurant = await Restaurant.create(req.body)

      return res.send({
        restaurant
      })
    } catch (error) {}
  },

  async options(req, res) {
    const currentOptions = await History.findOne({
      createdAt: {
        $gte: getToday().start,
        $lte: getToday().end
      }
    }).exec()

    if (!currentOptions) {
      const restaurants = await Restaurant.find()
        .sort('name')
        .exec()

      const currentOptions = await History.create({
        restaurants
      })

      return res.send({
        currentOptions
      })
    }

    const selected = currentOptions.restaurants.find(r => r.votes.find(v => v === req.user.id))

    const winner = new Date().getHours() > 11 ? selectWinner(currentOptions.restaurants) : undefined

    const options = await mergeCurrentWithWinners(currentOptions)

    return res.send({
      _id: currentOptions._id,
      createdAt: currentOptions.createdAt,
      options,
      selected,
      winner
    })
  },

  async vote(req, res) {
    const { restaurantId } = req.body

    const lastWinners = await selectLastWinners()

    if (lastWinners.find(winner => `${winner._id}` === restaurantId)) {
      return res.status(400).send({
        error: 'This restaurant has already been chosen this week'
      })
    }

    await History.findOne(
      {
        createdAt: {
          $gte: getToday().start,
          $lte: getToday().end
        },
        'restaurants._id': restaurantId
      },
      (err, document) => {
        const hasVoted = document.restaurants.find(r => r.votes.find(v => v === req.user.id))

        if (hasVoted) {
          return res.status(400).send({
            error: 'You already chose a restaurant today'
          })
        }
      }
    )

    const restaurant = await History.findOneAndUpdate(
      {
        createdAt: {
          $gte: getToday().start,
          $lte: getToday().end
        },
        'restaurants._id': restaurantId
      },
      {
        $push: {
          'restaurants.$.votes': req.user.id
        }
      }
    )

    return res.status(200).send({ restaurant })
  },

  async delete(req, res) {
    await Restaurant.findOneAndDelete({ id: req.id }).exec()
    return res.status(204)
  }
}
