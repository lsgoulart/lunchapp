import jwt from 'jsonwebtoken'

export default (req, res, next) => {
  const { authorization } = req.headers

  if (!authorization) return res.status(401).send({ error: 'No token provided' })

  const parts = authorization.split(' ')
  if (!parts.length === 2) res.status(401).send({ error: 'Invalid token' })

  const [type, token] = parts

  if (!/^Bearer$/i.test(type)) res.status(401).send({ error: 'Invalid token' })

  jwt.verify(token, process.env.APP_KEY, (err, decoded) => {
    if (err) res.status(401).send({ error: 'Invalid token' })

    req.user = decoded
  })

  return next()
}
