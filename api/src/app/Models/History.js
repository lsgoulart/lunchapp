import { Schema, model } from 'mongoose'
import { RestaurantSchema } from './Restaurant'

const HistorySchema = new Schema({
  restaurants: [RestaurantSchema],
  createdAt: {
    type: Date,
    default: Date.now
  }
})

export default model('History', HistorySchema)
