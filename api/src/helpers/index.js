import Joi from 'joi'
import jwt from 'jsonwebtoken'

export const validateAsync = (data, schema) =>
  new Promise(resolve => {
    Joi.validate(data, schema, err => {
      if (err) {
        return resolve({
          fails: true,
          messages: err.details
        })
      }
      return resolve({ fails: false })
    })
  })

export const generateToken = params =>
  jwt.sign(params, process.env.APP_KEY, {
    expiresIn: 86400
  })
