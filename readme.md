# LunchApp

Solução desenvolvida com NodeJS e MongoDB para teste da empresa DbServer.
O projeto consiste em uma API REST com cadastro e autenticação de usuários, listagem de restaurantes disponíveis no dia e votação.
Junto ao projeto encontram-se dois arquivos de configuração do Postman para testar a API:
```bash
src/LunchApp.postman_collection.json
src/LunchApp.postman_environment.json
```

#### Requisitos
`node: ^10.0.0`
`npm: ^5.6`

Após clonar o repositório, deve-se instalar as dependências do projeto no diretório `api` com o comando:
```bash
npm install
```

Com as dependências instaladas pode-se rodar o projeto:
```bash
npm start
```

O servidor é iniciado na porta `4000`

## Endpoints
#### Usuários
`/POST`  `/user/register`: Cadastro de usuários.

`/POST` `/user/login`: Autenticação de usuário.

#### Restaurantes
`/GET` `/restaurants`: Lista de todos os restaurantes cadastrados.

`/POST` `/restaurants/create`: Cadastro de restaurantes.

`/GET` `/restaurants/options`: Restaurantes disponíveis para o dia de hoje, este método cria uma nova lista de restaurantes baseada na lista de todos os restaurantes cadastrados juntamente com o dia corrente. Se o registro do dia já existir ele retorna o registro já criado juntamente com as informações de restaurante vencedor - a votação encerra as 11:00 - o restaurante selecionado pelo usuário logado e a disponibilidade do mesmo em relação ao histórico da semana, não possibilitando votar em um restaurante vencedor da semana corrente. *Este método requer autenticação*.

`/POST` `/restaurants/vote`: Usuário vota no restaurante de sua preferencia. Este método também verifica se o restaurante pode ser votado, caso não tenha sido escolhido durante a semana vigente. *Este método requer autenticação*.